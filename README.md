# CIS 322 Project 2 #
Student/editor: Bryan Carl

Maintained by Ram Durairajan, Steven Walton.


This is a siple webserver app using the Python Flask framework for web development. It is a very basic model only handling 403 and 404 errors on a basic level.

Instructions follow below on how to run the docker to get to the web server.



# How to build and run the web server

* Go to the web folder in the repository. Read every line of the docker file and the simple flask app.

* Build the simple flask app image using

  ```
  docker build -t UOCIS-flask-demo .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```

* Launch http://127.0.0.1:5000 using web broweser and check the output "UOCIS docker demo!"
