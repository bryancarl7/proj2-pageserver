from flask import Flask, render_template, request
import os


app = Flask(__name__)


# for the standard home page load
@app.route("/")
def index():
    return render_template("./Home.html"), 202


# for handling different requests
@app.route("/<string:name>")
def template_name(name):
    template_url = "templates/"+name

    # Check to make sure the file exists and mark flag appropriately
    if os.path.exists(template_url):
        print(True)
        flag = True
    else:
        flag = False

    # Make sure the path doesnt contain any bad characters
    # Call 403 if so
    if ('~' in name) or ("//" in name) or (".." in name):
        return forbidden_url()

    # Call 404
    elif not flag:
        return page_not_found()

    # Finally display page if it passes all checks
    else:
        print("check3")
        return render_template("./{}.html".format(name)), 202


# Error for handling bad URL paths
@app.errorhandler(403)
def forbidden_url():
    return render_template("./403.html"), 403


# Error for handling pages not found
@app.errorhandler(404)
def page_not_found():
    return render_template("./404.html"), 404


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
